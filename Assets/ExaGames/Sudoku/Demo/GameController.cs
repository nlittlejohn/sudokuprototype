﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using ExaGames.SudokuGenerator;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

// AdMob dependencies
using GoogleMobileAds.Api;

/// <summary>
/// Demo game controller for a Sudoku board.
/// </summary>
/// <author>ExaGames</author>
/// <description>
/// 1. Create a new board:
/// 	SudokuBoard sudokuBoard = new SudokuBoard();
/// 2. Get a random puzzle for a SudokuBoard:
/// 	int[,] puzzle = sudokuBoard.GetPuzzle();
/// 3. Check solution: compare your player inputs with the sudokuBoard.Board property.
/// </description>
public class GameController : MonoBehaviour {
	/// <summary>
	/// Timer label.
	/// </summary>
	public Text Timer;

	public int timerInt;

	/// <summary>
	/// Message label.
	/// </summary>
	public Text Message;
	// Arrays containing the input fields of the board.
	public InputField[]	Row1;
	public InputField[]	Row2;
	public InputField[]	Row3;
	public InputField[]	Row4;
	public InputField[]	Row5;
	public InputField[]	Row6;
	public InputField[]	Row7;
	public InputField[]	Row8;
	public InputField[]	Row9;

	/// <summary>
	/// Main sudoku board.
	/// </summary>
	private SudokuBoard sudokuBoard;
	/// <summary>
	/// Unsolved puzzle, with zero's in those positions which should be empty.
	/// </summary>
	private int[,] puzzle;
	/// <summary>
	/// Array of input fields arrays, for easy access inside loops.
	/// </summary>
	private InputField[][] fields;

    // Used for storing the time once a puzzle is solved
    private int solveTime;

    public Boolean solvePressed;
	public Boolean puzzleSolved = false;

    private int clearButtonPressedCount = 0;
	private int adThreshold = 0;
	private int AD_THRESHOLD_LIMIT = 3;
	private Boolean doCheckAdThreshold = true;

	// Modal Window properties. One 'yes' action for each button
	private ModalPanel modalPanel;
	private DisplayManager displayManager;
	private UnityAction newGameAction;
	private UnityAction clearBoardAction;
	private UnityAction solveBoardAction;
	private UnityAction myNoAction;

	// Win Messages (shown after clicking check button)
	private String[] winMessages = { "Congratulations!", "You Win!", "Great success!", "Winner!", "Great job!", "Victory!" };

	// Admob Initializations
	private InterstitialAd interstitial;

	void Awake () {
		modalPanel = ModalPanel.Instance ();
		displayManager = DisplayManager.Instance ();

		newGameAction = new UnityAction (OnNewGameButtonPressed);
		clearBoardAction = new UnityAction (OnClearButtonPressed);
		solveBoardAction = new UnityAction (OnSolveButtonPressed);
		myNoAction = new UnityAction (ModalNoAction);
	}
	
	void Start() {
		// Store references to the input field arrays for easy access inside loops.
		fields = new InputField[9][];
		fields[0] = Row1;
		fields[1] = Row2;
		fields[2] = Row3;
		fields[3] = Row4;
		fields[4] = Row5;
		fields[5] = Row6;
		fields[6] = Row7;
		fields[7] = Row8;
		fields[8] = Row9;

		// AdMob Initializations
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-3940256099942544/1033173712";
		#elif UNITY_IPHONE
		// Initialize the ad unit in "dev" mode but change it if it's not
		//string adUnitId = "ca-app-pub-3940256099942544/4411468910"; // Test id
		string adUnitId = "ca-app-pub-5537043926635507/7483301837";

		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Initialize the Google Mobile Ads SDK.
		MobileAds.Initialize(adUnitId);
		// Initialize an interstitial ad
		this.interstitial = new InterstitialAd(adUnitId);   
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder()
			.AddTestDevice("921690651876471e67b0b8c9a14deafc")
			.Build();
		// Load the interstitial with the request.
		this.interstitial.LoadAd(request);


		OnNewGameButtonPressed();
    }

	/// <summary>
	/// Show ModalPAnel with 'New Game' dialog.
	/// </summary>
	public void NewGameInitialPress() {
		modalPanel.Choice ("Would you like to start a new game?", newGameAction, myNoAction);
	}

	/// <summary>
	/// Show MOdalPanel with 'Clear Board' dialog.
	/// </summary>
	public void ClearInitialPress() {
		modalPanel.Choice ("This will clear the board, continue?", clearBoardAction, myNoAction);
	}

	/// <summary>
	/// Show MOdalPanel with 'Solve' dialog.
	/// </summary>
	public void SolveInitialPress() {
		modalPanel.Choice ("Solve the puzzle? Your score will not be recorded.", solveBoardAction, myNoAction);
	}

	void ModalNoAction() {
		// Do Nothing
	}

	/// <summary>
	/// Creates a new sudoku board.
	/// </summary>
	public void OnNewGameButtonPressed() {
		StopCoroutine("countTime");

        solvePressed = false;
        sudokuBoard = new SudokuBoard();	// Create a sudoku board.
		puzzle = sudokuBoard.GetPuzzle();	// Get a random puzzle for the created board.
		
		OnClearButtonPressed();
        ActivateOtherButtons();

        StartCoroutine("countTime");

		if (doCheckAdThreshold) {
			StartCoroutine ("checkAdThreshold");
		}

		// Ad stuff **********************************
		if (timerInt > 120) {
			adThreshold += 1;
			checkShowInterstitialAd ();
		}
	}

	/// <summary>
	/// Compares the player inputs with the board.
	/// </summary>
	public void OnCheckButtonPressed() {
		puzzleSolved = true;
		for(int i=0; i<9; i++) {
			for(int j = 0; j<9; j++) {
				if(string.IsNullOrEmpty(fields[i][j].text) || Convert.ToInt32(fields[i][j].text) != sudokuBoard.Board[i, j]) {
					puzzleSolved = false;
					break;
				}
			}

			if(!puzzleSolved) {
                if (GameControl.control.isSoundOn) {
                    GameObject clearButton = GameObject.Find("BtnCheckIncorrect");
                    clearButton.GetComponent<AudioSource>().Play();
                }

                break;
            }
		}
		if(!puzzleSolved) {
			Message.text = "Keep trying";
			// Reset the message text after a delay
			Invoke("resetMessageText", 3);
		} else {
            if (GameControl.control.isSoundOn) {
                GameObject clearButton = GameObject.Find("BtnCheck");
                clearButton.GetComponent<AudioSource>().Play();
            }


			puzzleSolved = true;
			int messageIndex = UnityEngine.Random.Range(0, winMessages.Count());
			Message.text = winMessages[messageIndex];

			StopCoroutine("countTime");
            // Record time
            //  PlayerPrefs.SetInt("solveTime", solveTime);
            GameControl.control.time = solveTime;

            // ************* Comment out 'if' statement to record high scores even if check is pressed *********
            if (!solvePressed) {
                incrementTotalWinCount();
                checkHighScores(solveTime);
            }

            DeactivateOtherButtons();
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // Go to Win Scene
        }
		Message.gameObject.SetActive(true);
	}

    public void DeactivateOtherButtons() {
        GameObject newButton = GameObject.Find("BtnNew");
        GameObject solveButton = GameObject.Find("BtnSolve");
        GameObject checkButton = GameObject.Find("BtnCheck");
        GameObject clearButton = GameObject.Find("BtnClear");
        GameObject proceedWinButton = GameObject.Find("BtnWinProceed");

        newButton.transform.position = new Vector3(-400, 49, 0);  //78 49
        solveButton.transform.position = new Vector3(-400, 49, 0);  //-94.5 62.5
        checkButton.transform.position = new Vector3(-400, 49, 0);  //-267.5 -388.5
        clearButton.transform.position = new Vector3(-400, 49, 0);  //231.5 -390
		//proceedWinButton.transform.position = new Vector3(600,450 , 0);
		proceedWinButton.transform.position = new Vector3((Screen.width / 2), (Screen.height / 6) , 0);
    }

    public void ActivateOtherButtons() {
        GameObject newButton = GameObject.Find("BtnNew");
        GameObject solveButton = GameObject.Find("BtnSolve");
        GameObject checkButton = GameObject.Find("BtnCheck");
        GameObject clearButton = GameObject.Find("BtnClear");

        newButton.transform.position = new Vector3(122, 2087, 0);  
        solveButton.transform.position = new Vector3(977, 2087, 0);
        checkButton.transform.position = new Vector3(924, 608, 0);
        clearButton.transform.position = new Vector3(144, 610, 0);
    }

    /// <summary>
    /// Copy the puzzle to the input fields.
    /// </summary>
    public void OnClearButtonPressed() {
        Color lightBlue = new Color32(0x00, 0x6F, 0xFF, 0xFF);
		for(int i=0; i<9; i++) {
			for(int j = 0; j<9; j++) {
				if(puzzle[i, j] > 0) {
					fields[i][j].text = puzzle[i, j].ToString();
					fields[i][j].interactable = false;
					fields[i][j].textComponent.color = lightBlue;
				} else {
					fields[i][j].text = string.Empty;
					fields[i][j].interactable = false;
					fields[i][j].textComponent.color = Color.black;
				}
			}
		}

        if (GameControl.control.isSoundOn && clearButtonPressedCount > 0) {
            GameObject clearButton = GameObject.Find("BtnClear");
            clearButton.GetComponent<AudioSource>().Play();
        }

        solvePressed = false;
        Message.gameObject.SetActive(false);
        clearButtonPressedCount++;
    }

	/// <summary>
	/// Copy the entire board to the input fields.
	/// </summary>
	public void OnSolveButtonPressed() {
		// Ad stuff **********************************
		if (timerInt > 120) {
			adThreshold += 3;
			checkShowInterstitialAd ();
		}

        solvePressed = true; // TODO disable this for testing ------------------------------
		StopCoroutine("countTime");
		Color lightBlue = new Color32(0x00, 0x6F, 0xFF, 0xFF);
		for(int i=0; i<9; i++) {
			for(int j = 0; j<9; j++) {
				if(puzzle[i, j] > 0) {
					fields[i][j].text = puzzle[i, j].ToString();
					fields[i][j].interactable = false;
					fields[i][j].textComponent.color = lightBlue;
				} else {
					fields[i][j].text = sudokuBoard.Board[i, j].ToString();
					fields[i][j].interactable = false;
					fields[i][j].textComponent.color = Color.black;
				}
			}
		}

        if (GameControl.control.isSoundOn) {
            GameObject clearButton = GameObject.Find("BtnCheck");
            clearButton.GetComponent<AudioSource>().Play();
        }

        Message.gameObject.SetActive(false);
	}

	/// <summary>
	/// Coroutine to count time and print it in the message label.
	/// </summary>
	/// <returns>The time.</returns>
	private IEnumerator countTime() {
		int timer = 0;
		Timer.text = string.Format("Time: {0:D2}:{1:D2}", 0, 0);
		while(true) {
			yield return new WaitForSeconds(1f);
			timer++;
			timerInt++;
			TimeSpan t = TimeSpan.FromSeconds(timer);
			Timer.text = string.Format("Time: {0:D2}:{1:D2}", t.Minutes, t.Seconds);
            solveTime = timer;
		}
	}

    /**
     * Check if the new high score needs added to the dictionary
     * If it is not a higher value than any of the existing scores, do nothing
     */
    private void checkHighScores(int solveTime) {
        String solveDate = DateTime.Now.ToString();

        if (GameControl.control.highScores == null) {
            // This is the first high score
            GameControl.control.highScores.Add(solveTime, solveDate);
        } else {
            // Check if the highscore needs inserted into list if we're at or > 10
            if (GameControl.control.highScores.Keys.Count >= 10) {
                // Check for duplicate score
                if (GameControl.control.highScores.ContainsKey(solveTime)) {
                    Debug.Log("Duplicate score found. Update value");
                    GameControl.control.highScores[solveTime] = solveDate;
                } else {
                    // Put keys into a list to sort
                    List<int> solveTimes = GameControl.control.highScores.Keys.ToList<int>();
                    // // Add the new score
                    solveTimes.Add(solveTime); // Add in the new solveTime and remove whatever is highest
                    GameControl.control.highScores.Add(solveTime, solveDate);
                    // Remove the highest time
                    GameControl.control.highScores.Remove(solveTimes.Max());
                }


            } else {
                // Not 10 scores yet, simply add it
                // Check for duplicate score
                if (GameControl.control.highScores.ContainsKey(solveTime)) {
                    Debug.Log("Duplicate score found. Update value");
                    GameControl.control.highScores[solveTime] = solveDate;
                } else {
                    GameControl.control.highScores.Add(solveTime, solveDate);
                }

            }
        }
    }

    /**
     * Increment the total win count held in GameControl.PlayerData
     */
    private void incrementTotalWinCount() {
        GameControl.control.totalWins++;
    }

	private void resetMessageText() {
		Message.text = "";
	}

	private void checkShowInterstitialAd() {
		if (timerInt > 120 && this.interstitial.IsLoaded () && (adThreshold >= AD_THRESHOLD_LIMIT)) {
			this.interstitial.Show ();
			adThreshold = 0; // Reset threshold
		} else if (timerInt > 120 && adThreshold >= AD_THRESHOLD_LIMIT) {
			AdRequest request = new AdRequest.Builder ()
				.AddTestDevice ("921690651876471e67b0b8c9a14deafc")
				.Build ();
			this.interstitial.LoadAd (request);
			this.interstitial.Show ();
			adThreshold = 0;
		} else {
			// Do nothing
		}
	}

	/// <summary>
	/// Coroutine to adjust threshold for ads Show ad when threshold is met.
	/// </summary>
	/// <returns>The time.</returns>
	private IEnumerator checkAdThreshold() {
		while(doCheckAdThreshold) {
			yield return new WaitForSeconds(10f);
			if (adThreshold > 0) {
				adThreshold--;
				Debug.Log ("Ad threshold is now at " + adThreshold);
			} else {
				Debug.Log ("Ad threshold is at: " + adThreshold);
			}
		}
	}
}