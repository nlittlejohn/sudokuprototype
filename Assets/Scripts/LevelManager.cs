﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

// AdMob dependencies
using GoogleMobileAds.Api;

public class LevelManager : MonoBehaviour {	

	// Admob Initializations
	private InterstitialAd interstitial;

	void Start () {
		// AdMob Initializations
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-3940256099942544/1033173712";
		#elif UNITY_IPHONE
			// Initialize the ad unit in "dev" mode but change it if it's not
			//string adUnitId = "ca-app-pub-3940256099942544/4411468910"; // Admob test id
			string adUnitId = "ca-app-pub-5537043926635507/7483301837";
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Initialize the Google Mobile Ads SDK.
		MobileAds.Initialize(adUnitId);
		// Initialize an interstitial ad
		this.interstitial = new InterstitialAd(adUnitId);   
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder()
			.AddTestDevice("921690651876471e67b0b8c9a14deafc")
			.Build();
		// Load the interstitial with the request.
		this.interstitial.LoadAd(request);
	}
    
	/**
     * Load a level by name
     * 
     * @parameter - level name
     */
    public void LoadLevel(string name){

        if(this.GetComponent<AudioSource>() != null) {
            Debug.Log("PLAY?");
            this.GetComponent<AudioSource>().Play();
        } else {
            Debug.Log("NO AUDIO SOURCE?");
        }

		Debug.Log ("New Level load: " + name);
        //SceneManager.LoadScene(name);
        DelayedLoad(name, 0.25f);
	}

    /**
     * Load the next level in the build settings order
     */
	public void LoadNextLevel() {

        if (this.GetComponent<AudioSource>() != null) {
            this.GetComponent<AudioSource>().Play();
        }

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void DelayedLoad(string name, float delayTime) {
        StartCoroutine(LoadLevelEnum(name, delayTime));
	}

	/**
	 * Load a level after showing an ad
	 * Delegate to DelayedLoad
	 */
	public void LoadLevelAfterAd (string name) {
		if (this.interstitial.IsLoaded ()) {
			this.interstitial.Show ();
		} else {
			AdRequest request = new AdRequest.Builder()
				.AddTestDevice("921690651876471e67b0b8c9a14deafc")
				.Build();
			this.interstitial.LoadAd (request);
			this.interstitial.Show ();
		}

		// FOR TEST PURPOSES ONLY - to get device id
		if (Debug.isDebugBuild && UnityEngine.iOS.Device.advertisingIdentifier != null) {
			Debug.Log ("!!! Device ID: " + GetIOSAdMobID ());
		} else {
			Debug.Log("!!! Device identifier null");
		}

		DelayedLoad (name, 0.25f);
	}


    public IEnumerator LoadLevelEnum(string name, float delayTime) {
        yield return new WaitForSeconds(delayTime);

        SceneManager.LoadScene(name);
    }


	// FOR TEST PURPOSES ONLY - to get device id ------------------------------------------------------------------
	public static string Md5Sum(string strToEncrypt) {
		System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
		byte[] bytes = ue.GetBytes(strToEncrypt);

		System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
		byte[] hashBytes = md5.ComputeHash(bytes);

		string hashString = ""; 
		for (int i = 0; i < hashBytes.Length; i++) {
			hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
		}

		return hashString.PadLeft(32, '0');
	}

	public static string GetIOSAdMobID() {
		return Md5Sum(UnityEngine.iOS.Device.advertisingIdentifier);

	}
	// --------------------------------------------------------------------------------------------------------------
}
