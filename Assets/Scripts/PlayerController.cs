﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
//using UnityEditor;

using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
    
	public UISelection uiSelection = UISelection.NONE;
    public Image inputHighlight;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    /**
     * Get the current UI selection (0-9, NONE, DELETE, UNDO
     */
    public string getUISelection() {
        return uiSelection.ToString();
    }

    /**
     * Set the player's current number - what will be input into the board
     * at the selected tile. (Player selects 1 and clicks on a tile - tile will be set to 1)
     */
	public void setUISelection(string selection) {
		Debug.Log("Set UI Selection to: " + selection);

		switch (selection) {
			case "1":
				uiSelection = UISelection.ONE;
				GameObject one_selector = GameObject.Find ("1 Selector");
				inputHighlight.transform.position = one_selector.GetComponent<RectTransform> ().position;
				one_selector.GetComponent<AudioSource>().Play();
                break;
			case "2":
				uiSelection = UISelection.TWO;
				GameObject two_selector = GameObject.Find ("2 Selector");
				inputHighlight.transform.position = two_selector.GetComponent<RectTransform> ().position;

                if (GameControl.control.isSoundOn) {
                    two_selector.GetComponent<AudioSource>().Play();
                }

                break;
			case "3":
				uiSelection = UISelection.THREE;
				GameObject three_selector = GameObject.Find ("3 Selector");
				inputHighlight.transform.position = three_selector.GetComponent<RectTransform> ().position;

                if (GameControl.control.isSoundOn) {
                    three_selector.GetComponent<AudioSource>().Play();
                }

                break;
			case "4":
				uiSelection = UISelection.FOUR;
				GameObject four_selector = GameObject.Find ("4 Selector");
				inputHighlight.transform.position = four_selector.GetComponent<RectTransform> ().position;

                if (GameControl.control.isSoundOn) {
                    four_selector.GetComponent<AudioSource>().Play();
                }

                break;
			case "5":
				uiSelection = UISelection.FIVE;
				GameObject five_selector = GameObject.Find ("5 Selector");
				inputHighlight.transform.position = five_selector.GetComponent<RectTransform> ().position;

                if (GameControl.control.isSoundOn) {
                    five_selector.GetComponent<AudioSource>().Play();
                }

                break;
			case "6":
				uiSelection = UISelection.SIX;
				GameObject six_selector = GameObject.Find ("6 Selector");
				inputHighlight.transform.position = six_selector.GetComponent<RectTransform> ().position;

                if (GameControl.control.isSoundOn) {
                    six_selector.GetComponent<AudioSource>().Play();
                }

                break;
			case "7":
				uiSelection = UISelection.SEVEN;
				GameObject seven_selector = GameObject.Find ("7 Selector");
				inputHighlight.transform.position = seven_selector.GetComponent<RectTransform> ().position;

                if (GameControl.control.isSoundOn) {
                    seven_selector.GetComponent<AudioSource>().Play();
                }

                break;
			case "8":
				uiSelection = UISelection.EIGHT;
				GameObject eight_selector = GameObject.Find ("8 Selector");
				inputHighlight.transform.position = eight_selector.GetComponent<RectTransform> ().position;

                if (GameControl.control.isSoundOn) {
                    eight_selector.GetComponent<AudioSource>().Play();
                }

                break;
			case "9":
				uiSelection = UISelection.NINE;
				GameObject nine_selector = GameObject.Find ("9 Selector");
				inputHighlight.transform.position = nine_selector.GetComponent<RectTransform> ().position;

                if (GameControl.control.isSoundOn) {
                    nine_selector.GetComponent<AudioSource>().Play();
                }

                break;
			case "delete":
				uiSelection = UISelection.DELETE;
                GameObject delete_selector = GameObject.Find("Delete Selector");
                inputHighlight.transform.position = delete_selector.GetComponent<RectTransform>().position;

                if (GameControl.control.isSoundOn) {
                    delete_selector.GetComponent<AudioSource>().Play();
                }

                break;
            case "undo":
                undo();
                break;
			default:
				uiSelection = UISelection.NONE;
                inputHighlight.transform.position = new Vector3(-400, 70, 0);
                break;
		}
    }

    public void undo() {
        Debug.Log("Undo action");
    }


	/**
     *  Enum used to set which UI element is selected
     */
	public enum UISelection {
		NONE,
		ONE,
		TWO,
		THREE,
		FOUR,
		FIVE,
		SIX,
		SEVEN,
		EIGHT,
		NINE,
		DELETE
	}
}
