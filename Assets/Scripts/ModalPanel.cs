﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ModalPanel : MonoBehaviour {

	public Text question;
	public Image iconImage;
	public Button yesButton;
	public Button noButton;
	public GameObject modalPanelObject;

	private static ModalPanel modalPanel;

	public static ModalPanel Instance() {
		if (!modalPanel) {
			modalPanel = FindObjectOfType (typeof(ModalPanel)) as ModalPanel;
			if (!modalPanel) {
				Debug.LogError ("There needs to be one active ModalPanel script on a GameObject in your scene.");
			}
		}

		return modalPanel;
	}

	public void Choice (string question, UnityAction yesEvent, UnityAction noEvent) {
		modalPanelObject.SetActive (true);

		// Remove previous listeners
		yesButton.onClick.RemoveAllListeners();
		//Set up our listeners
		yesButton.onClick.AddListener(yesEvent);
		//yesButton.onClick.AddListener (ClosePanel ());
		yesButton.onClick.AddListener(ClosePanel);

		// Remove previous listeners
		noButton.onClick.RemoveAllListeners();
		//Set up our listeners
		noButton.onClick.AddListener(noEvent);
		noButton.onClick.AddListener (ClosePanel);

		this.question.text = question;
		this.iconImage.gameObject.SetActive (true);
		yesButton.gameObject.SetActive (true);
		noButton.gameObject.SetActive (true);
	}

	void ClosePanel () {
		modalPanelObject.SetActive (false);
	}
}
