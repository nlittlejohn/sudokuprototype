﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class WinSceneController : MonoBehaviour {

    public Text time;

    private static int solveTime;

    // Use this for initialization
    void Start() {
        Debug.Log("WinScene Start");
        int timeValue = GameControl.control.time;
        Debug.Log("Time Value: " + timeValue);
        TimeSpan t = TimeSpan.FromSeconds(timeValue);
        Debug.Log("Time Span: " + t.Minutes + ":" + t.Seconds);
        time.text = string.Format("Time: {0:D2}:{1:D2}", t.Minutes, t.Seconds);
    }

    void OnGUI() {
        // Set the position high score table
		int xPos = (Screen.width / 10);
        int yPos = Screen.height / 2;
        // Set the Font Size/Color of high score table
		GUIStyle labelStyle = new GUIStyle();
		labelStyle.fontSize = 48;
		labelStyle.normal.textColor = Color.white;
        // Increment counter for numbering of high scores
        int counter = 1;

        List<int> solveTimes = GameControl.control.highScores.Keys.ToList<int>();
		solveTimes.Sort();

        // Obtain the previous high score as a TimeSpam and format it to hh:mm:SS
        foreach (int key in solveTimes) {
			string keyValue = GameControl.control.highScores[key];

			TimeSpan previousTime = TimeSpan.FromSeconds(key);
			string previousTimeFormatted = string.Format("{0:D2}:{1:D2}:{2:D2}", previousTime.Hours, previousTime.Minutes, previousTime.Seconds);

			// Apply the high score table
            if (counter < 10) {
                // Align scores for when the counter is 2 digits
                GUI.Label(new Rect(xPos, yPos, 250, 30), "  " + counter + ". " + previousTimeFormatted + "      " + keyValue, labelStyle);
            } else {
                GUI.Label(new Rect(xPos, yPos, 250, 30), counter + ". " + previousTimeFormatted + "      " + keyValue, labelStyle);
            }
			yPos += 54; // Move position of next score row down
			counter++; // Increment counter
		}

        //    // Obtain the previous high score as a TimeSpam and format it to hh:mm:SS:ss
        //foreach (KeyValuePair<int, string> keyValue in GameControl.control.highScores) {
        //    TimeSpan previousTime = TimeSpan.FromSeconds(keyValue.Key);
        //    string previousTimeFormatted = string.Format("{0:D2}:{1:D2}:{2:D2}:{3:D3}", previousTime.Hours, previousTime.Minutes, previousTime.Seconds, previousTime.Milliseconds);

        //   // Apply the high score table
        //    GUI.Label(new Rect(xPos, yPos, 250, 30), counter + ". " + previousTimeFormatted + "      " + keyValue.Value, labelStyle);
        //    yPos += 54; // Move position of next score row down
        //    counter++; // Increment counter
        //}
    }

    // Update is called once per frame
    void Update() {

    }
}
