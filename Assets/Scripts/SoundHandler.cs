﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundHandler : MonoBehaviour {
    public Sprite soundOn;
    public Sprite soundOff;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Turn sound on/off
    public void setMuteStatus() {
        GameControl.control.isSoundOn = !GameControl.control.isSoundOn;
        GameControl.control.Save();

        GameObject muteButton = GameObject.Find("MuteButton");

        if (muteButton.GetComponent<Image>().sprite.name == "soundOn") {
            //If the sound is ON, turn the sprite to the "soundOff" sprite
            muteButton.GetComponent<Image>().sprite = soundOff;
        } else if (muteButton.GetComponent<Image>().sprite.name == "soundOff") {
            //If the sound is OFF, turn the sprite to the "soundOn" sprite
            muteButton.GetComponent<Image>().sprite = soundOn;
        }
        
        muteButton.GetComponent<AudioSource>().Play();
    }
}
