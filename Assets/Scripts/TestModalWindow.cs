﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TestModalWindow : MonoBehaviour {

	private ModalPanel modalPanel;
	private DisplayManager displayManager;

	private UnityAction myYesAction;
	private UnityAction myNoAction;

	void Awake () {
		modalPanel = ModalPanel.Instance ();
		displayManager = DisplayManager.Instance ();

		myYesAction = new UnityAction (TestYesFunction);
		myNoAction = new UnityAction (TestNoFunction);
	}

	// 	Send to the ModalPanel to set up the Buttons and functions to call
	public void TestYesOrNo() {
		modalPanel.Choice ("Would you like burger?\nHow about a hotdog?", myYesAction, myNoAction);
	}

	//	These are wrapped in UnityActions
	void TestYesFunction() {
		displayManager.DisplayMessage ("Heck yeah!");
	}

	void TestNoFunction() {
		displayManager.DisplayMessage ("NYOPE!");
	}
}
