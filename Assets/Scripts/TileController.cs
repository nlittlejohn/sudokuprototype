﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
//using UnityEditor;

public class TileController : MonoBehaviour, IPointerClickHandler {
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayTileSound() {
		if (GameControl.control.isSoundOn) {
			foreach (AudioSource audio in this.GetComponentsInParent(typeof(AudioSource))) {
				audio.Play ();
			}
		}
	}

    public void OnPointerClick (PointerEventData eventData) {
        Debug.Log("setTileText: " + GameObject.Find("PlayerInputHandler").GetComponent<PlayerController>().getUISelection());

        var inputField = this.gameObject.GetComponent<InputField>();

		Color lightBlue = new Color32(0x00, 0x6F, 0xFF, 0xFF);
		if (inputField.textComponent.color == lightBlue) {
            // Do nothing, this is a pre-set tile
            Debug.Log("Cannot reassign value on this tile");
            return;
        }

		GameObject canvas = GameObject.Find ("Canvas");
		if (canvas.GetComponent<GameController>().puzzleSolved == true) {
			return;
		}

        switch (GameObject.Find("PlayerInputHandler").GetComponent<PlayerController>().getUISelection()) {
			case "ONE":
				Debug.Log ("Set tile [] to 1");
				inputField.text = "1";
				PlayTileSound();
                break;
            case "TWO":
                Debug.Log("Set tile [] to 2");
				inputField.text = "2";
				PlayTileSound();
                break;
            case "THREE":
                Debug.Log("Set tile [] to 3");
				inputField.text = "3";
				PlayTileSound();
                break;
            case "FOUR":
                Debug.Log("Set tile [] to 4");
				inputField.text = "4";
				PlayTileSound();
                break;
            case "FIVE":
                Debug.Log("Set tile [] to 5");
				inputField.text = "5";
				PlayTileSound();
                break;
            case "SIX":
                Debug.Log("Set tile [] to 6");
				inputField.text = "6";
				PlayTileSound();
                break;
            case "SEVEN":
                Debug.Log("Set tile [] to 7");
				inputField.text = "7";
				PlayTileSound();
                break;
            case "EIGHT":
                Debug.Log("Set tile [] to 8");
				inputField.text = "8";
				PlayTileSound();
                break;
            case "NINE":
                Debug.Log("Set tile [] to 9");
				inputField.text = "9";
				PlayTileSound();
                break;
            case "DELETE":
                Debug.Log("Set tile [] to empty");
				inputField.text = "";
				PlayTileSound();
                break;
            default:
                Debug.Log("DEFAULT: Set tile [] to empty");
                inputField.text = "";
                break;
        }
    }

    ///**
    // * Set the tile to the text set in the PlayerInputHandler
    // * (The player chose 0-9 or to delete the tile)
    // */
    //public void setText() {
    //    Debug.Log("setTileText: " + GameObject.Find("PlayerInputHandler").GetComponent<PlayerController>().getUISelection());

    //    //var button = GameObject.Find("testTile").GetComponent<Button>();
    //    //button.GetComponentInChildren<Text>().text = GameObject.Find("PlayerInputHandler").GetComponent<PlayerController>().getUISelection();

    //    var inputField = this.gameObject.GetComponent<Text>();
    //    Debug.Log("this: " + this.gameObject.name);

    //    // Debug.Log("UI SELECTION in setText(): " + GameObject.Find("PlayerInputHandler").GetComponent<PlayerController>().getUISelection());

    //    switch (GameObject.Find("PlayerInputHandler").GetComponent<PlayerController>().getUISelection()) {
    //        case "ONE":
    //            Debug.Log("Set tile [] to 1");
    //            this.GetComponentInParent<Text>().text = "1";
    //            break;
    //        case "TWO":
    //            Debug.Log("Set tile [] to 2");
    //            inputField.GetComponentInChildren<Text>().text = "2"; ;
    //            break;
    //        case "THREE":
    //            Debug.Log("Set tile [] to 3");
    //            inputField.GetComponentInChildren<Text>().text = "3";
    //            break;
    //        case "FOUR":
    //            Debug.Log("Set tile [] to 4");
    //            inputField.GetComponentInChildren<Text>().text = "4";
    //            break;
    //        case "FIVE":
    //            Debug.Log("Set tile [] to 5");
    //            inputField.GetComponentInChildren<Text>().text = "5";
    //            break;
    //        case "SIX":
    //            Debug.Log("Set tile [] to 6");
    //            inputField.GetComponentInChildren<Text>().text = "6";
    //            break;
    //        case "SEVEN":
    //            Debug.Log("Set tile [] to 7");
    //            inputField.GetComponentInChildren<Text>().text = "7";
    //            break;
    //        case "EIGHT":
    //            Debug.Log("Set tile [] to 8");
    //            inputField.GetComponentInChildren<Text>().text = "8";
    //            break;
    //        case "NINE":
    //            Debug.Log("Set tile [] to 9");
    //            inputField.GetComponentInChildren<Text>().text = "9";
    //            break;
    //        case "DELETE":
    //            Debug.Log("Set tile [] to empty");
    //            inputField.GetComponentInChildren<Text>().text = "";
    //            break;
    //        default:
    //            inputField.GetComponentInChildren<Text>().text = "";
    //            break;
    //    }
    //}
}
