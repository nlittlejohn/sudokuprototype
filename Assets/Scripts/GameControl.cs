﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameControl : MonoBehaviour {

    public static GameControl control;

    public int time;
    public int totalWins = 0;
    public Dictionary<int, String> highScores = new Dictionary<int, string>();
    public Boolean solveButtonPressed = false;
    public Boolean isSoundOn = true;

    void Awake () {
        if (control == null) {
            DontDestroyOnLoad(gameObject);
            control = this;
        } else if (control != this) {
            Destroy(gameObject);
        }
    }

    void OnGUI() {
        //GUI.Label(new Rect(10, 10, 100, 30), "Current time:" + time);
    }

    public void Save() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

        PlayerData data = new PlayerData();
        data.currentSolveTime = time;
        data.totalWins = totalWins;
        data.highScores = highScores;
        data.isSoundOn = isSoundOn;

        bf.Serialize(file, data);
        file.Close();
    }

    public void Load() {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            PlayerData data = (PlayerData) bf.Deserialize(file);
            file.Close();

            time = data.currentSolveTime;
            //highScores = topSolveTimeList;
        }
    }
}

[Serializable]
class PlayerData {
    public int currentSolveTime;
    public int totalWins;
    public Dictionary<int, String> highScores;
    public Boolean isSoundOn;
}
