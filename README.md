# README #

### What is this repository for? ###

* Quick summary
* Version

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

Background Color: 006FFFFF

How to build and deploy:
[https://unity3d.com/learn/tutorials/topics/mobile-touch/building-your-unity-game-ios-device-testing]

Admob Plugin:
* Must use workspace for unity project and NOT the xcode project
* Must set Build settings of Pods project to <= 10 for iOS target

Icons:
[http://modernuiicons.com]
[https://ionicons.com]
